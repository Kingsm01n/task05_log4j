
import org.apache.logging.log4j.*;

public class Log4jExample {

    private static final Logger log =  LogManager.getLogger(Log4jExample.class);

    public static void main(String[] args) throws Exception{
        try{
            throw new Exception();
        }catch (Exception e) {
            log.trace("this is trace message");
            log.debug("This is debug message");
            log.info("This is info message");
            log.warn("This is warn message");
            log.error("this is error message");
            log.fatal("This is fatal message");
        }
    }
}
